import React from 'react';
import Authentication from './components/Authentication/Authentication'

function App() {
  return (
    <div className="App">
      <Authentication/>
    </div>
  );
}

export default App;
