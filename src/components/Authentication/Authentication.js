import React, { Component } from "react";
import "./Authentication.css";
import fluidLogo from "../../assets/group-20@2x.png";
import { Line } from "rc-progress";
import axios from "axios";

export default class Authentication extends Component {
  state = {
    authenticate: "Authenticate",
    dataLoad: 0,
    modelLoad: 0,
    trainingLoad: 0,
    id: "",
    trainStatus: ""
  };

  dataLoader = () => {
    if (this.state.authenticate === "Authenticated") {
      var interval = setInterval(
        function() {
          if (this.state.dataLoad === 100) {
            this.modelLoader();
            clearInterval(interval);
          }
          if (this.state.dataLoad !== 100) {
            this.setState({ dataLoad: this.state.dataLoad + 1 });
          }
        }.bind(this),
        100
      );
    }
  };

  modelLoader = () => {
    if (this.state.dataLoad === 100) {
      var interval = setInterval(
        function() {
          if (this.state.modelLoad === 100) {
            clearInterval(interval);
          }
          if (this.state.modelLoad !== 100) {
            this.setState({ modelLoad: this.state.modelLoad + 1 });
          }
        }.bind(this),
        100
      );
    }
  };

  authenticate = () => {
    this.setState({ authenticate: "Authenticating.." });
    setTimeout(
      function() {
        //Start the timer
        this.setState({ authenticate: "Authenticated" });
        if (this.state.authenticate === "Authenticated") {
          this.dataLoader();
        }
      }.bind(this),
      2000
    );
  };

  trainModel = () => {
    const config = { headers: { "Content-Type": "multipart/form-data" } };

    axios
      .post("http://35.200.253.15/createjob/", {}, config)
      .then(data => {
        // console.log(data.data.jobs)
        this.setState({
          trainingLoad: data.data.jobs.percentage,
          id: data.data.jobs.id_job,
          trainStatus: "Training.."
        });
        this.progressModel();
      })
      .catch(error => {
        alert(error.response.data.Response);
        console.log(error);
      });
  };

  progressModel = () => {
    var interval = setInterval(
      function() {
        if (this.state.trainingLoad === 100) {
          this.setState({ trainStatus: "Trained" });
          clearInterval(interval);
        }
        if (this.state.trainingLoad !== 100) {
          const config = { headers: { "Content-Type": "multipart/form-data" } };

          if (this.state.trainingLoad === 80) {
            setTimeout(
              function() {
                axios
                  .put("http://35.200.253.15/createjob/", {}, config)
                  .then(data => {
                    // console.log(data.data.jobs)
                    this.setState({
                      trainingLoad: data.data.jobs.percentage
                    });
                  })
                  .catch(error => {
                    alert(error.response);
                    console.log(error);
                  });
              }.bind(this),
              7000
            );
          } else {
            axios
              .put("http://35.200.253.15/createjob/", {}, config)
              .then(data => {
                // console.log(data.data.jobs)
                this.setState({
                  trainingLoad: data.data.jobs.percentage
                });
              })
              .catch(error => {
                alert(error.response);
                console.log(error);
              });
          }
        }
      }.bind(this),
      1000
    );
  };

  render() {
    return (
      <div className="authentication-container">
        <header className="demo-page-header">
          <img className="fluid-logo" src={fluidLogo} alt="fluid-logo" />
        </header>
        <div className="authentication-body-container">
          <div className="action-box-heading"></div>
          <div className="action-body">
            <div className="action-box">
              <h1>Heading for fete Learning</h1>
              <button
                onClick={this.authenticate}
                className="authenticate-button"
              >
                Start
              </button>
              <div
                className={
                  this.state.authenticate !== "Authenticate"
                    ? "auth-button-box"
                    : "invisible"
                }
              >
                <h3>
                  {this.state.authenticate}
                  <span
                    className={
                      this.state.authenticate === "Authenticated"
                        ? "auth-check"
                        : "invisible"
                    }
                  >
                    &#10003;
                  </span>
                </h3>
              </div>
              <div
                className={
                  this.state.authenticate === "Authenticated"
                    ? "data-loading-container"
                    : "invisible"
                }
              >
                <h3>
                  Loading local data
                  <span
                    className={
                      this.state.dataLoad === 100 ? "auth-check" : "invisible"
                    }
                  >
                    &#10003;
                  </span>
                </h3>

                <Line
                  percent={`${this.state.dataLoad}`}
                  strokeWidth="2"
                  strokeColor="red"
                />
              </div>
              <div
                className={
                  this.state.dataLoad === 100
                    ? "globas-model-container"
                    : "invisible"
                }
              >
                <h3>
                  Downloading global model update
                  <span
                    className={
                      this.state.modelLoad === 100 ? "auth-check" : "invisible"
                    }
                  >
                    &#10003;
                  </span>
                </h3>
                <Line
                  percent={`${this.state.modelLoad}`}
                  strokeWidth="2"
                  strokeColor="green"
                />
              </div>
              <div
                className={
                  this.state.modelLoad === 100 ? "train-container" : "invisible"
                }
              >
                <button className="training-button" onClick={this.trainModel}>
                  Start Training
                </button>
                <h3 className={this.state.id !== "" ? "" : "invisible"}>
                  {this.state.trainStatus}
                  <span
                    className={
                      this.state.trainingLoad === 100
                        ? "auth-check"
                        : "invisible"
                    }
                  >
                    &#10003;
                  </span>
                </h3>
                <Line
                  className={this.state.id !== "" ? "" : "invisible"}
                  percent={`${this.state.trainingLoad}`}
                  strokeWidth="2"
                  strokeColor="blue"
                />
              </div>
              <h3 className={this.state.trainingLoad >= 79 ? "" : "invisible"}>
                Sending gradient securely
                <span
                  className={
                    this.state.trainingLoad === 80 ? "saving" : "invisible"
                  }
                >
                  .
                </span>
                <span
                  className={
                    this.state.trainingLoad === 80 ? "saving" : "invisible"
                  }
                >
                  .
                </span>
                <span
                  className={
                    this.state.trainingLoad === 80 ? "saving" : "invisible"
                  }
                >
                  .
                </span>
                <span
                  className={
                    this.state.trainingLoad > 81 ? "auth-check" : "invisible"
                  }
                >
                  &#10003;
                </span>
              </h3>
            </div>
          <div className = "action-description">
                  <h1>Description</h1>
                  <p>Here will be the description Machine learning (ML) models is the most commonly used in a data science project. In this post, you will learn about different definitions of a machine learning model to get a better understanding of what are machine learning models</p>
          </div>
          </div>
        </div>
      </div>
    );
  }
}
